<html>
<head>
    <title>Laravel tutorial</title>
</head>
<body>
<h1>Posts application</h1>
<nav>
    Hello {{ \Illuminate\Support\Facades\Auth::user()->name }} ({{ \Illuminate\Support\Facades\Auth::user()->email }})
    <form method="post" action="/logout">
        {{ csrf_field() }}
        <button type="submit">Logout</button>
    </form>
</nav>
@yield('content')
</body>
</html>