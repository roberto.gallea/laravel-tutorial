@extends('layout')

@section('content')
    @include('errors')
    <!-- Posts table -->
    <table style="width: 100%; border: 1px solid black;">
        <tr>
            <th>Title</th>
            <th>Author</th>
            <th>Content</th>
            <th>Creation date</th>
            <th>Actions</th>
        </tr>
        @foreach($posts as $post)
            <tr>
                <td>{{ $post->title }}</td>
                <td>{{ $post->author->name }}</td>
                <td>{{ $post->content }}</td>
                <td>{{ $post->created_at }}</td>
                <td>
                    @can('delete-post',$post)
                    <form method="post" action="/post/{{ $post->uuid }}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button type="submit">Delete post</button>
                    </form>
                    @else
                        <button type="button" disabled>Delete post</button>
                    @endcan
                </td>
            </tr>
        @endforeach
    </table>


    <!-- New post form -->
    <form method="post" action="/post">
        {{ csrf_field() }}
        <table>
            <tr>
                <td><label for="title">Title</label></td>
                <td><input type="text" id="title" name="title"/><br/></td>
            </tr>
            <tr>
                <td><label for="content">Content</label></td>
                <td><textarea rows="10" cols="100" id="content" name="content"></textarea><br/></td>
            </tr>
            <tr>
                <td></td>
                <td><button type="submit">Create post</button></td>
            </tr>
        </table>
    </form>
@endsection