<!-- error page -->
@if (count($errors) > 0)
<h2>Errors!</h2>
<ul>
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</ul>
@endif