<?php

namespace App\Listeners;

use App\Events\PostDeletedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Storage;

class BackupDeletedPost
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PostDeletedEvent  $event
     * @return void
     */
    public function handle(PostDeletedEvent $event)
    {
        Storage::put('post_' . $event->post->id . '.txt', $event->post->content);
    }
}
