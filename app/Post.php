<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['title', 'content'];

    public function author()
    {
        return $this->belongsTo(User::class,'author_id','id');
    }

    public function getRouteKeyName()
    {
        return 'uuid';
    }
}
