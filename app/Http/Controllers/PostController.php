<?php

namespace App\Http\Controllers;

use App\Events\PostDeletedEvent;
use App\Http\Requests\DeletePostRequest;
use App\Http\Requests\StorePostRequest;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $posts = Post::orderBy('created_at','desc')->get();

        return view('posts')->with([
            'posts' => $posts,
        ]);
    }

    public function store(StorePostRequest $request) {
        $post = new Post();
        $post->uuid = Uuid::uuid4();
        $post->title = $request->title;
        $post->content = $request->content;
        $post->author_id = Auth::user()->id;
        $post->save();

        return redirect('/');
    }

    public function destroy(DeletePostRequest $request, Post $post) {
        $post->delete();
        event(new PostDeletedEvent($post));
        return redirect('/');
    }
}
