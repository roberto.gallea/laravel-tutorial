<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;

class AllowEveryFourSeconds
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $time = Carbon::now();
        if (($time->second % 4) !== 0)
            abort(403,'Time is ' . $time->format('H:i:s'));
        return $next($request);
    }
}
