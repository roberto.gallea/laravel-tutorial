<?php

use \Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PostController@index');

Route::post('/post', 'PostController@store');

Route::delete('/post/{post}', 'PostController@destroy');

Route::get('/test', function () {
    return 'ok';
})->middleware(['four_seconds']);

Route::get('/test2', function () {
    return 'invert this text';
})->middleware(['invert']);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
